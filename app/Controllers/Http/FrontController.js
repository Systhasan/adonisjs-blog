"use strict";

class FrontController {
  async index({ view }) {
    return view.render("frontEnd.home");
  }
}

module.exports = FrontController;
